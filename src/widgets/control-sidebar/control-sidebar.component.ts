import { Component, Input, Renderer2 } from "@angular/core";

@Component({
  selector: "app-aside",
  styleUrls: ["./control-sidebar.component.css"],
  templateUrl: "./control-sidebar.component.html"
})
export class ControlSidebarComponent {
  constructor(private renderer: Renderer2) {
    this.renderer.addClass(document.body, "sidebar-collapse");
    this.renderer.addClass(document.body, "sidebar-mini");
  }
}

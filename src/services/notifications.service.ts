
// based on https://github.com/ng-book/angular2-rxjs-chat/blob/master/app/ts/services/NotificationsService.ts
import {Notification, NotificationType} from '../models/notification';
import { Injectable } from '@angular/core';
import { Observable, Subject, ReplaySubject } from 'rxjs/Rx';

const initialNotifications: Notification[] = [];

type INotificationsOperation = (notifications: Notification[]) => Notification[];

@Injectable()
export class NotificationsService {
  private notificationsList: Notification[] = [];
  // a stream that publishes new notifications only once
  public newNotifications: Subject<Notification[]> = new Subject<
    Notification[]
    >();

  // `notifications` is a stream that emits an array of the most up to date notifications
  public notifications: ReplaySubject<Notification[]> = new ReplaySubject<Notification[]>(1);

  // `updates` receives _operations_ to be applied to our `notifications`
  // it's a way we can perform changes on *all* notifications (that are currently
  // stored in `notifications`)
  public updates: Subject<any> = new Subject<any>();

  // action streams
  public create: Subject<Notification> = new Subject<Notification>();
  // public markThreadAsRead: Subject<any> = new Subject<any>();

  constructor() {
    // recois des operation, et les fais sur la liste interne, puis diffuse le resultat sur notifications
    this.updates.subscribe((ope) => {
      this.notificationsList = ope(this.notificationsList);
      console.log(this.notificationsList);
      this.notifications.next(this.notificationsList);
    });

    this.newNotifications
      .map(function (
        newNotifications: Notification[]
      ): INotificationsOperation {
        return (notifications: Notification[]) => {
          if (newNotifications.length > 0) {
            notifications = notifications.filter(
              (oldNotification) =>
                oldNotification.type !== newNotifications[0].type
            );
            return notifications.concat(newNotifications);
          }
          return notifications;
        };
      })
      .subscribe(this.updates);

  }

  // an imperative function call to this action stream
  // make sure Notification[] has the same "type" for each element in the array
  public addNotifications(notifications: Notification[]): void {
    this.newNotifications.next(notifications);
  }

  public cleanNotification(notificationType: NotificationType): void {
    this.notificationsList = this.notificationsList.filter(
      (notification) => notification.type !== notificationType
    );

    this.notifications.next(this.notificationsList);
  }

}

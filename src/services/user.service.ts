import { User } from '../models/user';
import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs/Rx';
import { Router } from '@angular/router';

@Injectable()
export class UserService {
    public currentUser: ReplaySubject<User> = new ReplaySubject<User>( 1 );
    public imageStudiesIntervalID: any;
    public purchaseIntervalID: any;

    constructor(
      private router: Router
    ) {
      // TODO
    }

    public setCurrentUser( user: User ) {
      this.currentUser.next( user );
    }

    public logout() {
      if(this.imageStudiesIntervalID){
        clearInterval(this.imageStudiesIntervalID)
      }

      if (this.purchaseIntervalID) {
        clearInterval(this.purchaseIntervalID);
      }

      localStorage.clear();
      const user = new User();
      user.connected = false;
      this.setCurrentUser( user );
      this.router.navigate(['login']);
    }

    public setImageIntervalID(imageStudiesIntervalID: any){
      this.imageStudiesIntervalID = imageStudiesIntervalID;
    }

  public setPurchaseIntervalID(purchaseIntervalID: any) {
    this.purchaseIntervalID = purchaseIntervalID;
  }
}

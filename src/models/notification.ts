export type NotificationType = 'REQUISITION_PENDING'| 'STUDIES_TO_AUTHORIZE'
export class Notification {
  public content: string;
  public class: string;
  public link: string;
  public type: NotificationType;

  public constructor(data: any = {}) {
    this.content = data.content || '';
    this.class = data.class || '';
    this.link = data.link || '';
    this.type = data.type || '';
  }
}
